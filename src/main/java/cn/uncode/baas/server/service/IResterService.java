package cn.uncode.baas.server.service;

import java.util.List;
import java.util.Map;

import cn.uncode.baas.server.dto.RestAdmin;
import cn.uncode.baas.server.dto.RestApp;
import cn.uncode.baas.server.dto.RestGroup;
import cn.uncode.baas.server.dto.RestMethod;
import cn.uncode.baas.server.dto.RestRole;
import cn.uncode.baas.server.dto.RestTable;
import cn.uncode.baas.server.dto.RestUser;
import cn.uncode.baas.server.dto.RestUserAcl;


public interface IResterService {
	
	RestApp loadRestApp(String bucket);
	
	RestMethod loadRestMethod(String bucket, String name, String option, String version);
	
	RestTable loadRestTable(String bucket, String name);
	
	List<Map<String, Object>> loadRestTables(String bucket);
	
	List<RestAdmin> loadRestAdmin(String bucket, String table);
	
	
	
	/**
	 * 
	 * @param bucket 
	 * @param names 多个用,分隔
	 * @return
	 */
	List<RestGroup> loadRestGroups(String bucket, String names);
	
	RestGroup loadRestGroup(String bucket, String name);
	
	List<String> loadRestRolesFromGroup(String bucket, String names);
	
	int insertRestGroup(String bucket, String name, String desc, String roles);
	
	int updateRestGroup(String bucket, String name, String desc, String roles);
	
	int deleteRestGroup(String bucket, String name);
	
	int deleteRestGroup(int id);
	
	int countRestGroup(String bucket, String name);
	
	List<RestRole> loadRestRoles(String bucket, String names);
	
	RestRole loadRestRole(String bucket, String name);
	
	int insertRestRole(String bucket, String name, String desc);
	
	int updateRestRole(String bucket, String name, String desc);
	
	int deleteRestRole(String bucket, String name);
	
	int deleteRestRole(int id);
	
	void createDataBase(String dbName, String user, String pwd);
	
	
	RestUserAcl loadRestUserAcl(String bucket, String username);
	
	int insertRestUserAcl(String bucket, String username, String groups, String roles);
	
	int updateRestUserAcl(String bucket, String username, String groups, String roles);
	
	int deleteRestUserAcl(String bucket, String username);
	
	int deleteRestUserAcl(int id);
	
	int countRestUserAcl(String bucket, String username);
	
	
	RestUser loadRestUser(String bucket);
	
	int insertRestUser(String bucket, String table, String nameField, String passField, String statusField, boolean emailAuth, 
			String defaultGroup, String emailField, String mobileField);
	
	int updateRestUser(int id, String bucket, String table, String nameField, String passField, String statusField, boolean emailAuth, 
			String defaultGroup, String emailField, String mobileField);

}
